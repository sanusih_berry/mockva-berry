package com.daksa.mockva.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, Boolean> negative;
	public static volatile SingularAttribute<User, String> address;
	public static volatile SingularAttribute<User, BigDecimal> money;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, String> userid;
	public static volatile SingularAttribute<User, Date> birthDate;

}

