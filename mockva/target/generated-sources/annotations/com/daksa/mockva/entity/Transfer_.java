package com.daksa.mockva.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Transfer.class)
public abstract class Transfer_ {

	public static volatile SingularAttribute<Transfer, User> userSender;
	public static volatile SingularAttribute<Transfer, BigDecimal> nominal;
	public static volatile SingularAttribute<Transfer, Integer> id;
	public static volatile SingularAttribute<Transfer, User> userReceiver;
	public static volatile SingularAttribute<Transfer, Date> trfDate;

}

