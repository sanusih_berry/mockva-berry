/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Berry
 */
@Entity
@Table(name = "ttransfer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transfer.findAll", query = "SELECT t FROM Transfer t ORDER BY t.id"),
    @NamedQuery(name = "Transfer.findTransferByDate", query = "SELECT SUM(t.nominal), t.trfDate FROM Transfer t GROUP BY t.trfDate ORDER BY t.trfDate")})
public class Transfer implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @SequenceGenerator(name = "trfSeq", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Column(name = "id")
    private int id;    
    @ManyToOne
    @JoinColumn(name = "idSender")
    private User userSender;    
    @ManyToOne
    @JoinColumn(name = "idReceiver")
    private User userReceiver;
    @Column(name = "nominal")
    @NotNull
    private BigDecimal nominal;
    @Column(name = "trfDate")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date trfDate;    
            
    public User getUserSender() {
        return userSender;
    }

    public void setUserSender(User userSender) {
        this.userSender = userSender;
    }

    public User getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(User userReceiver) {
        this.userReceiver = userReceiver;
    }

    public BigDecimal getNominal() {
        return nominal;
    }
   
    public void setNominal(BigDecimal nominal) {
        this.nominal = nominal;
    }

    public Date getTrfDate() {
        return trfDate;
    }

    public void setTrfDate(Date trfDate) {
        this.trfDate = trfDate;
    }            
}
