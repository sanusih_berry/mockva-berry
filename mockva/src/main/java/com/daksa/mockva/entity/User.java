/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Berry
 */
@Entity
@Table(name = "tuser")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT t FROM User t ORDER BY t.userid"),
    @NamedQuery(name = "User.findByUserId", query = "SELECT t FROM User t WHERE t.userid = :userid"),    
    @NamedQuery(name = "User.findAllowNegative", query = "SELECT t.negative FROM User t WHERE t.userid = :userid")})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "userId", length = 6)
    private String userid;
    
    @NotNull
    @Column(name = "name", length = 50)
    private String name;
        
    @Column(name = "address", length = 100)
    private String address;
    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    
    @NotNull
    @Column(name = "money")
    private BigDecimal money;
    
    @NotNull
    @Column(name = "negative", nullable = false)
    private boolean negative;
        
    /*@OneToMany(mappedBy = "idSender")
    private List<Transfer> transferList;*/

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }
}
