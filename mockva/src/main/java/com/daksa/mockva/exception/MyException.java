/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.exception;

import javax.ejb.ApplicationException;
/**
 *
 * @author Berry
 */
@ApplicationException(rollback = true)
public class MyException extends Exception{
    public MyException() {
        super();
    }
    
    public MyException(String message) {
        super(message);
    }
}
