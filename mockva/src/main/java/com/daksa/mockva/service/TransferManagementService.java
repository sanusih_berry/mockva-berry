/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.service;

import com.daksa.mockva.entity.Transfer;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.daksa.mockva.entity.User;
import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.repository.TransferRepository;
import com.daksa.mockva.repository.UserRepository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
/**
 *
 * @author Berry
 */
@Stateless
public class TransferManagementService implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    private EntityManager entityManager;
    @Inject
    private TransferRepository transferRepository;
    @Inject
    private UserManagementService userManagementService;
    @Inject
    private UserRepository userRepository;
    
    public void createTransfer(Transfer transfer) {
        entityManager.persist(transfer);
    }   
    
    public List<Transfer> getAllTransfer() {
        List<Transfer> allTransfer = transferRepository.getAllTransfer();        
        return allTransfer;
    }
    
    public void doTransfer(String idSender, String idReceiver, BigDecimal nominalTransfer) throws MyException{
        User userSender = userRepository.findUserById(idSender);        
        User userReceiver = userRepository.findUserById(idReceiver);                         
                
        
        if ((idSender == null) || (idReceiver == null) || (nominalTransfer == null)) {
            throw new MyException("Transfer Gagal");
        }
        if (idSender.equals(idReceiver)) {            
            throw new MyException("Transfer Gagal");
        }
        if ((userSender == null) || (userReceiver == null)) {
            throw new MyException("Transfer Gagal");
        }    
        if ((userSender.getMoney().compareTo(nominalTransfer) < 0) && (userSender.isNegative() == false)) {
            throw new MyException("Transfer Gagal");
        }
                
        userSender.setMoney(userSender.getMoney().subtract(nominalTransfer));
        userReceiver.setMoney(userReceiver.getMoney().add(nominalTransfer));
        
        userManagementService.updateUser(userSender);
        userManagementService.updateUser(userReceiver);
        
        Transfer trf = new Transfer();   
        trf.setUserSender(userSender);
        trf.setUserReceiver(userReceiver);        
        trf.setNominal(nominalTransfer);
        trf.setTrfDate(new Date());
        createTransfer(trf);                
    }        
}

