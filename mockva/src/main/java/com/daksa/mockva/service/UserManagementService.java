/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.service;

import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.daksa.mockva.entity.User;
import com.daksa.mockva.repository.UserRepository;
import java.math.BigDecimal;
import java.util.Date;
import com.daksa.mockva.exception.MyException;

/**
 *
 * @author Berry
 */
@Stateless
public class UserManagementService implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject 
    private EntityManager entityManager;
    @Inject
    private UserRepository userRepository;         
    
    public void createUser(User user) {
        entityManager.persist(user);
    }
    
    public void updateUser(User user) {
        entityManager.merge(user);
    }                  
    
    public void registerUser(String id, String name, String address, Date birthDate, BigDecimal money, boolean negative) throws MyException{
        User existingUser;       
        
        existingUser = userRepository.findUserById(id);
        if (existingUser == null) {            
            User newUser = new User();            
            newUser.setUserid(id);
            newUser.setName(name);
            newUser.setAddress(address);
            newUser.setBirthDate(birthDate);
            if (money == null) {
                newUser.setMoney(new BigDecimal(0));
            }
            else {
                newUser.setMoney(money); 
            }            
            newUser.setNegative(negative);
            createUser(newUser);                        
        } 
        else {
            throw new MyException("User ID Sudah Ada");
        }
    }
}
