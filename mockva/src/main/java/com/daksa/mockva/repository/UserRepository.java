/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.repository;

import com.daksa.mockva.entity.User;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Berry
 */
@Dependent
public class UserRepository implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject
    private EntityManager entityManager;
    
    public List<User> getAllUser() {        
        TypedQuery<User> query = entityManager.createNamedQuery("User.findAll", User.class);          
        return query.getResultList();
    }
    
    public User findUserById(String userid) {
        try {
            TypedQuery<User> query = entityManager.createNamedQuery("User.findByUserId", User.class);
            query.setParameter("userid", userid);            
            return query.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }                
    }
    
    /*public List getAllowNegative(String userid) {
    }*/
}
