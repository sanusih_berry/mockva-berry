/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.repository;

import com.daksa.mockva.entity.Transfer;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author Berry
 */
@Dependent
public class TransferRepository implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject
    private EntityManager entityManager;
    
    public List<Transfer> getAllTransfer() {        
        TypedQuery<Transfer> query = entityManager.createNamedQuery("Transfer.findAll", Transfer.class);                     
        return query.getResultList();
    }
    
    public List getTransferByDate() {        
        TypedQuery<Object[]> query = entityManager.createNamedQuery("Transfer.findTransferByDate", Object[].class);
        return query.getResultList();
    }
}
