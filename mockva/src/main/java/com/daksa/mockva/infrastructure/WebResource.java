/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.infrastructure;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Berry
 */
@Dependent
public class WebResource {    
    @Produces
    @RequestScoped
    public FacesContext produceFacesContext() {
	return FacesContext.getCurrentInstance();
    }       
}
