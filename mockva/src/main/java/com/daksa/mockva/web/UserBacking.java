/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.entity.User;
import com.daksa.mockva.repository.UserRepository;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class UserBacking implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject
    private UserRepository userRepository;
    
    private User user;
    private List<User> allUser;
    private List<User> filteredUser;

    public User getUser() {
        return user;
    }  
    
    public List<User> getAllUser() {
        allUser = userRepository.getAllUser();        
        return allUser;
    }

    public List<User> getFilteredUser() {
        return filteredUser;
    }

    public void setFilteredUser(List<User> filteredUser) {
        this.filteredUser = filteredUser;
    }
}
