/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.repository.TransferRepository;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.Axis;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class ChartBacking implements Serializable {
    private static final long serialVersionUID = 1L;    
    private LineChartModel cartesianChartModel;   
    
    @Inject
    private TransferRepository transferRepository;
    
    //private Transfer allTransfer;
    
    @PostConstruct
    public void initChart() {
        createLineModel();
    }
    
    public void createLineModel() {
        cartesianChartModel = new LineChartModel();        
        LineChartSeries series = new LineChartSeries();
        cartesianChartModel.setTitle("Transfer");
        cartesianChartModel.setLegendPosition("e");
        cartesianChartModel.getAxes().put(AxisType.X, new CategoryAxis("Tanggal"));        
        Axis yAxis = cartesianChartModel.getAxis(AxisType.Y);                        
        List dataTransfer = transferRepository.getTransferByDate();        
        int i;        
        for (i = 0; i < dataTransfer.size(); i++) {
            Object[] trf = (Object[]) dataTransfer.get(i);                    
            series.set(trf[1].toString(), (Number)trf[0]);            
        }        
        cartesianChartModel.addSeries(series);               
    }            

    public LineChartModel getCartesianChartModel() {
        return cartesianChartModel;
    }

    public void setCartesianChartModel(LineChartModel cartesianChartModel) {
        this.cartesianChartModel = cartesianChartModel;
    }
}
