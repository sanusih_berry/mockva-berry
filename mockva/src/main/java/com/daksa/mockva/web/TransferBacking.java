/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.entity.Transfer;
import com.daksa.mockva.entity.User;
import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.repository.UserRepository;
import com.daksa.mockva.service.TransferManagementService;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class TransferBacking implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject
    private UserRepository userRepository;    
    @Inject
    private TransferManagementService transferManagementService;    
    @Inject 
    private ChartBacking chartBacking;
    @Inject
    private FacesContext facesContext;    
    
    private String idSender;
    private String idReceiver;
    private BigDecimal nominalTransfer;
    private List<Transfer> allTransfer;    

    public String getIdSender() {
        return idSender;
    }

    public void setIdSender(String idSender) {
        this.idSender = idSender;
    }

    public String getIdReceiver() {
        return idReceiver;
    }

    public void setIdReceiver(String idReceiver) {
        this.idReceiver = idReceiver;
    }

    public BigDecimal getNominalTransfer() {
        return nominalTransfer;
    }

    public void setNominalTransfer(BigDecimal nominalTransfer) {
        this.nominalTransfer = nominalTransfer;
    }
    
    public List<Transfer> getAllTransfer() {        
        allTransfer = transferManagementService.getAllTransfer();        
        return allTransfer;
    }

    public void setAllTransfer(List<Transfer> allTransfer) {
        this.allTransfer = allTransfer;
    }        
    
    public String getUserName() {        
        User user = userRepository.findUserById(idReceiver);
        if (user != null) {
            return user.getName();
        }
        else {
            return "(.....)";
        }                                             
    }
    
    public void doTransfer() {  
        try {
            transferManagementService.doTransfer(idSender, idReceiver, nominalTransfer);
            //DataTable tableTrf = (DataTable)facesContext.getViewRoot().findComponent(":listTrf:tableTrf");            
            //tableTrf.resetValue();
            //chartBacking.createLineModel();
            facesContext.addMessage(null, new FacesMessage("Success", "Transfer Berhasil"));
        }
        catch(MyException e) {
            facesContext.addMessage(null, new FacesMessage("Failed", e.getMessage()));
        }                
    }            
}
