/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.service.UserManagementService;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Berry
 */
@Named
@ViewScoped
public class RegistrationBacking implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Inject
    private UserManagementService userManagementService;   
    @Inject
    private FacesContext facesContext;    
    
    private String id;
    private String name;
    private String address;
    private Date birthDate;
    private BigDecimal money;
    private boolean negative;             
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    
    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }
    
    public void registerUser() {   
        try {
            userManagementService.registerUser(id, name, address, birthDate, money, negative);           
            //DataTable tableUser = (DataTable)facesContext.getViewRoot().findComponent(":listUser:tableUser");            
            //tableUser.resetValue();                
            facesContext.addMessage(null, new FacesMessage("Success", "Data Berhasil Disimpan"));                                                    
        }        
        catch(MyException e) {
            facesContext.addMessage(null, new FacesMessage("Failed", e.getMessage()));                                    
        }        
    }                     
}
