/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.service;

import com.daksa.mockva.entity.User;
import org.junit.Test;
import com.daksa.mockva.repository.UserRepository;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
/**
 *
 * @author Berry
 */
@RunWith(MockitoJUnitRunner.class)
public class userManagementServiceTes {    
    @Mock
    UserRepository userRepository;
    @Mock
    UserManagementService userManagementService;                   
    
    @Test
    public void registerUserTest() { 
        User user = null;
        Mockito.when(userRepository.findUserById("dummy3")).thenReturn(user);
        User newUser = new User();   
        newUser.setUserid("dummy3");
        newUser.setName("dummy3");
        newUser.setAddress(null);
        newUser.setBirthDate(null);
        newUser.setMoney(new BigDecimal(1000));
        newUser.setNegative(true);     
        Mockito.doNothing().when(userManagementService).createUser(newUser);
        userManagementService.createUser(newUser);    
        //Assert.assertEquals(newUser.getUserid(), user.getUserid());
        Assert.assertNotSame(newUser, user);
    }
    
    @Test
    public void registerUserTest2() {         
        User user = new User();
        user.setUserid("dummy1");
        user.setName("dummy1");
        user.setAddress(null);
        user.setBirthDate(null);
        user.setMoney(new BigDecimal(1000));
        user.setNegative(true);
        Mockito.when(userRepository.findUserById("dummy1")).thenReturn(user);
        
        User newUser = new User();   
        newUser.setUserid("dummy1");
        newUser.setName("dummy1");
        newUser.setAddress(null);
        newUser.setBirthDate(null);
        newUser.setMoney(new BigDecimal(1000));
        newUser.setNegative(true);             
        Mockito.doNothing().when(userManagementService).createUser(newUser);
        userManagementService.createUser(newUser);
        Assert.assertEquals(newUser.getUserid(), user.getUserid());
    }
    
    @Test
    public void updateUserTest() {
        User user1 = new User();           
        user1.setUserid("dummy1");
        BigDecimal moneyBeforeUpdate = user1.getMoney();
        user1.setMoney(new BigDecimal(10000));
        BigDecimal moneyAfterUpdate = user1.getMoney();
        Mockito.doNothing().when(userManagementService).updateUser(user1);
        Assert.assertNotEquals(moneyAfterUpdate, moneyBeforeUpdate);
    }
}
