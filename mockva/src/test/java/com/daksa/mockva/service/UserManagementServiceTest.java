/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.service;

import com.daksa.mockva.entity.User;
import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.repository.UserRepository;
import com.daksa.mockva.service.UserManagementService;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
/**
 *
 * @author Berry
 */
@RunWith(MockitoJUnitRunner.class)
public class UserManagementServiceTest {
    @InjectMocks
    UserManagementService userManagementService;
    @Mock
    UserRepository userRepository;
    @Mock
    EntityManager entityManager;
    
     @Test
    public void registerUserTest() throws MyException {
        userManagementService.registerUser("dummy3", "dummy3", null, null, new BigDecimal(10000), true);
        verify(entityManager).persist(Mockito.any(User.class));
    }

    @Test(expected = MyException.class)
    public void registerUserTest2() throws MyException {
        User user = new User();
        user.setUserid("dummy3");                
        when(userRepository.findUserById("dummy3")).thenReturn(user);         
        userManagementService.registerUser("dummy3", "dummy3", null, null, new BigDecimal(10000), true);                        
    }
}
