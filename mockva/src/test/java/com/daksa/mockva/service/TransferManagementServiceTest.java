/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.service;

import com.daksa.mockva.entity.Transfer;
import com.daksa.mockva.entity.User;
import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.repository.TransferRepository;
import com.daksa.mockva.repository.UserRepository;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
/**
 *
 * @author Berry
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferManagementServiceTest {
    @Mock 
    private EntityManager entityManager;
    @Mock
    private TransferRepository transferRepository;
    @Mock
    private UserManagementService userManagementService;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private TransferManagementService transferManagementService;        
    
    User sender, receiver;
    
    @Before
    public void setUp() {
        sender = new User();
        sender.setUserid("dummy1");
        receiver = new User();
        receiver.setUserid("dummy1");
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_allInputNull() throws MyException {
        transferManagementService.doTransfer(null, null, null);
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_idReceiverNominalNull() throws MyException {
        transferManagementService.doTransfer("dummy1", null, null);
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_nominalNull() throws MyException {
        transferManagementService.doTransfer("dummy1", "dummy2", null);
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_idSenderNull() throws MyException {
        transferManagementService.doTransfer(null, "dummy2", new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_idSenderIdReceiverNull() throws MyException {
        transferManagementService.doTransfer(null, null, new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_idSenderIdReceiverSame() throws MyException {
        transferManagementService.doTransfer("dummy1", "dummy1", new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_userSenderNotFound() throws MyException {
        when(userRepository.findUserById("dummy1")).thenReturn(null);
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_userReceiverNotFound() throws MyException {
        when(userRepository.findUserById("dummy2")).thenReturn(null);
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_userSenderUserReceiverNotFound() throws MyException {
        when(userRepository.findUserById("dummy1")).thenReturn(null);
        when(userRepository.findUserById("dummy2")).thenReturn(null);
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest_nominalTooBig() throws MyException {        
        when(userRepository.findUserById("dummy1")).thenReturn(sender);
        sender.setMoney(new BigDecimal(5000));
        sender.setNegative(false);
        when(userRepository.findUserById("dummy2")).thenReturn(receiver);
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
    }
    
    @Test
    public void doTransferTest1() throws MyException {        
        when(userRepository.findUserById("dummy1")).thenReturn(sender);
        sender.setMoney(new BigDecimal(5000));
        sender.setNegative(true);
        when(userRepository.findUserById("dummy2")).thenReturn(receiver);      
        receiver.setMoney(new BigDecimal(20000));
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
        verify(entityManager).persist(Mockito.any(Transfer.class));
    }
    
    @Test
    public void doTransferTest2() throws MyException {        
        when(userRepository.findUserById("dummy1")).thenReturn(sender);
        sender.setMoney(new BigDecimal(20000));
        sender.setNegative(true);
        when(userRepository.findUserById("dummy2")).thenReturn(receiver);      
        receiver.setMoney(new BigDecimal(20000));
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
        verify(entityManager).persist(Mockito.any(Transfer.class));
    }
    
    @Test
    public void doTransferTest3() throws MyException {        
        when(userRepository.findUserById("dummy1")).thenReturn(sender);
        sender.setMoney(new BigDecimal(20000));
        sender.setNegative(false);
        when(userRepository.findUserById("dummy2")).thenReturn(receiver);      
        receiver.setMoney(new BigDecimal(20000));
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
        verify(entityManager).persist(Mockito.any(Transfer.class));
    }
}
