/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.entity.User;
import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.repository.UserRepository;
import com.daksa.mockva.service.TransferManagementService;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
/**
 *
 * @author Berry
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferBackingTest {
    @Mock
    private UserRepository userRepository;    
    @Mock
    private TransferManagementService transferManagementService;    
    @Mock
    private ChartBacking chartBacking;
    @Mock
    private FacesContext facesContext;
    @InjectMocks
    private TransferBacking transferBacking;
    
    @Test
    public void doTransferTest() throws MyException {
        transferBacking.setIdSender("dummy1");
        transferBacking.setIdReceiver("dummy2");
        transferBacking.setNominalTransfer(new BigDecimal(10000));
        transferBacking.doTransfer();        
        ArgumentCaptor<FacesMessage> captor = ArgumentCaptor.forClass(FacesMessage.class);
        verify(facesContext).addMessage(Mockito.anyString(), captor.capture());       
        FacesMessage message = captor.getValue();
        Assert.assertEquals("Success", message.getSummary());
        Assert.assertEquals("Transfer Berhasil", message.getDetail());
    }
    
    @Test(expected = MyException.class)
    public void doTransferTest2() throws MyException {
        transferBacking.setIdSender("dummy1");
        transferBacking.setIdReceiver("dummy2");
        transferBacking.setNominalTransfer(new BigDecimal(10000));
        transferBacking.doTransfer();        
        doThrow(new MyException("Transfer Gagal")).when(transferManagementService).doTransfer("dummy1", "dummy2", new BigDecimal(10000));
        transferManagementService.doTransfer("dummy1", "dummy2", new BigDecimal(10000));
        ArgumentCaptor<FacesMessage> captor = ArgumentCaptor.forClass(FacesMessage.class);
        verify(facesContext).addMessage(Mockito.anyString(), captor.capture());       
        FacesMessage message = captor.getValue();
        Assert.assertEquals("Failed", message.getSummary());
        Assert.assertEquals("Transfer Gagal", message.getDetail());
    }
    
    @Test
    public void getUserNameTest() {
        transferBacking.setIdReceiver("dummy2");
        User user = new User();
        user.setUserid("dummy2");
        user.setName("dummy2");
        when(userRepository.findUserById("dummy2")).thenReturn(user);
        String result = transferBacking.getUserName();
        Assert.assertEquals("dummy2", result);
    }
    
    @Test
    public void getUserNameTest2() {
        transferBacking.setIdReceiver("dummy2");        
        when(userRepository.findUserById("dummy2")).thenReturn(null);
        String result = transferBacking.getUserName();
        Assert.assertEquals("(.....)", result);
    }
}
