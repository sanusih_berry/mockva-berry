/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.mockva.web;

import com.daksa.mockva.exception.MyException;
import com.daksa.mockva.service.UserManagementService;
import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
/**
/**
 *
 * @author Berry
 */
@RunWith(MockitoJUnitRunner.class)
public class RegistrationBackingTest {
    @Mock
    private UserManagementService userManagementService;    
    @Mock
    private FacesContext facesContext;
    @InjectMocks
    private RegistrationBacking registrationBacking;
       
    @Test
    public void registerUserTest() throws MyException {    
        registrationBacking.setId("dummy1");
        registrationBacking.setName("dummy1");
        registrationBacking.setMoney(BigDecimal.ONE);
        registrationBacking.setNegative(true);
        registrationBacking.setAddress("bandung");                   
        registrationBacking.registerUser();            
        ArgumentCaptor<FacesMessage> captor = ArgumentCaptor.forClass(FacesMessage.class);
        verify(facesContext).addMessage(Mockito.anyString(), captor.capture());       
        FacesMessage message = captor.getValue();
        Assert.assertEquals("Success", message.getSummary());
        Assert.assertEquals("Data Berhasil Disimpan", message.getDetail());
    }
    
    @Test(expected = MyException.class)
    public void RegisterUserTest2() throws MyException {
        registrationBacking.setId("dummy1");
        registrationBacking.setName("dummy1");
        registrationBacking.setMoney(BigDecimal.ONE);
        registrationBacking.setNegative(true);
        registrationBacking.setAddress("bandung");
        registrationBacking.registerUser();          
        doThrow(new MyException("ID Sudah Ada")).when(userManagementService).registerUser("dummy1", "dummy1", "bandung", null, BigDecimal.ONE, true);
        userManagementService.registerUser("dummy1", "dummy1", "bandung", null, BigDecimal.ONE, true);
        ArgumentCaptor<FacesMessage> captor = ArgumentCaptor.forClass(FacesMessage.class);
        verify(facesContext).addMessage(Mockito.anyString(), captor.capture());       
        FacesMessage message = captor.getValue();
        Assert.assertEquals("Failed", message.getSummary());
        Assert.assertEquals("User ID Sudah Ada", message.getDetail());
    }
}
